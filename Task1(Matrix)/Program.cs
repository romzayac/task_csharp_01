﻿using System;

namespace Task1_Matrix_
{
    class Matrix
    {

        public Matrix(int rowsCount, int columnsCount, double[,]twodimensionalArray)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            matrix = (double[,])twodimensionalArray.Clone();
        }

        //Create matrix mxn and evaluate all values zero
        public Matrix(int rowsCount,int columnsCount)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            matrix = new double[rowsCount, columnsCount];
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    matrix[i, j] = 0;
                }
            }
        }

        public double this[int i, int j]
        {
            get
            {
                return this.matrix[i, j];
            }
            set
            {
                matrix[i, j] = value;
            }
        }

        public double GetMinorOfOrder(int order)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            double[,] subMatr = new double[order,order];

            for (int i = 0; i < order; i++)
            {
                for (int j = 0; j < order; j++)
                {
                    subMatr[i, j] = this.matrix[i, j];
                }
            }

            return GetDeterminantValue(subMatr);
        }

        public double GetSupplementaryMinor(int order)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            int subMatrixOrder = this.ColumnsCount-order;

            double[,] subMatrix = new double[subMatrixOrder, subMatrixOrder];

            int subMatrixRowNumber = 0;
            for(int i=subMatrixOrder;i>=order;i--)
            {
                int subMatrixColumnNumber = 0;
                for (int j = subMatrixOrder; j >= order; j--)
                {
                    subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = this.matrix[i, j];
                    subMatrixColumnNumber++;
                }
                subMatrixRowNumber++;
            }

            return GetDeterminantValue(subMatrix);
        }

        public void Show()
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public double GetMinorOfElement(int iCoordinate,int jCoordinate)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            int subMatrixOrder = this.ColumnsCount - 1;

            double[,] subMatrix = new double[subMatrixOrder, subMatrixOrder];

            int subMatrixRowNumber = 0;
            for (int i = 0; i < this.ColumnsCount; i++)
            {
                if (i == iCoordinate && i < this.ColumnsCount-1 )
                    i++;

                    int subMatrixColumnNumber = 0;
                    for (int j = 0; j < this.RowsCount; j++)
                    {
                        if (j == jCoordinate && j < this.ColumnsCount -1)
                            j++;

                            subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = this.matrix[i, j];
                            subMatrixColumnNumber++;
                        
                    }
                    subMatrixRowNumber++;
                
            }

            return GetDeterminantValue(subMatrix);
        }

        private static double GetDeterminantValue(double[,] matrix)
        {

            int order = int.Parse(Math.Sqrt(matrix.Length).ToString());
            
            if (order == 2)
            {
                return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            }
            else if (order > 2)
            {
                double determinantValue = 0;

                for (int i = 0; i < order; i++)
                {
                    double[,] subMatrix = CreateSubMatrix(matrix, 0, i);
                    determinantValue += ElementSign(0, i) * matrix[0, i] * GetDeterminantValue(subMatrix);
                }
                return determinantValue;
            }
            else
            {
                return matrix[0, 0];
            }
        }

        private static double[,] CreateSubMatrix(double[,] matrix, int rowsCount, int columnsCount)
        {
            int order = int.Parse(Math.Sqrt(matrix.Length).ToString());

            double[,] subMatrix = new double[order - 1, order - 1];

            int subMatrixRowNumber = 0;
            int subMatrixColumnNumber;
            for (int rowNumber = 0; rowNumber < order; rowNumber++, subMatrixRowNumber++)
            {
                if (rowNumber != rowsCount)
                {
                    subMatrixColumnNumber = 0;
                    for (int columnNumber = 0; columnNumber < order; columnNumber++)
                    {
                        if (columnNumber != columnsCount)
                        {
                            subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = matrix[rowNumber, columnNumber];
                            subMatrixColumnNumber++;
                        }
                    }
                }
                else
                {
                    subMatrixRowNumber--;
                }
            }
            return subMatrix;
        }

        private static double ElementSign(int rowIndex, int columnIndex)
        {
            return Math.Pow(-1, rowIndex + columnIndex);
        }

        public int RowsCount
        {
            get
            {
                return _rowsCount;
            }
            private set
            {
                _rowsCount = value;
            }

        }

        public int ColumnsCount
        {
            get
            {
                return _columnsCount;
            }
            private set
            {
                _columnsCount = value;
            }
        }

        public bool IsSquare
        {
            get
            {
                if (RowsCount == ColumnsCount)
                    return true;
                else
                    return false;
            }
        }

        private double[,] matrix;
        private int _rowsCount;
        private int _columnsCount;

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\t\t************Task1(Matrix)************\n\n");
            try
            {
                double[,] twoDimArray1 = new double[3, 3] { { 2, 1, 3 }, { 5, 7, 5 }, { 1, 3, 17 } };
                Matrix matrixA = new Matrix(3, 3, twoDimArray1);
                Console.WriteLine("Matrix A:");
                matrixA.Show();

                double minor = matrixA.GetMinorOfOrder(2);
                Console.WriteLine("\nMinor of {0}-th order in A: {1}", 2, minor);

                int order2 = 1;
                double suppMinor = matrixA.GetSupplementaryMinor(order2);
                Console.WriteLine("\nSupplementary minor to minor {0}-th order: {1}", order2, suppMinor);

                double elMinor = matrixA.GetMinorOfElement(1, 0);
                Console.WriteLine("\nMinor of element with coordinates [{0},{1}]: {2}\n", 1, 0, elMinor);

                Console.WriteLine("{0}+{1}*{2} = {3} - in this example i used only minors, so they can add, multiply, substract and divide\n", minor, suppMinor, elMinor, (minor + suppMinor * elMinor));

                double[,] twoDimArray2 = new double[5, 5] { { 21, 12, 35, 41, 14 }, { 5, 7, 5, 99, 17 }, { 1, 3, 17, 12, 101 }, { 33, 31, 37, 36, 35 }, { 121, 82, 1, 13, 1000 } };
                Matrix matrixB = new Matrix(5, 5, twoDimArray2);
                Console.WriteLine("Matrix B:");
                matrixB.Show();

                double minorB = matrixB.GetMinorOfOrder(5);
                Console.WriteLine("\nMinor of {0}-th order in B: {1} (if you dont believe, try calculate it on paper and compare!)\n", 5, minorB);
            }
            catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                double[,] twoDimArray3 = new double[2, 3] { { 1, 2, 3 }, { 4, 5, 7 } };
                Matrix matrixC = new Matrix(2, 3, twoDimArray3);
                Console.WriteLine("Matrix C:");
                matrixC.Show();

                double minorOfElement = matrixC.GetMinorOfElement(1, 2);
                Console.WriteLine(minorOfElement);
            }
            catch(ArgumentException e)
            {
                Console.WriteLine("\n"+e.Message+" "+e.StackTrace);
            }

        }
    }
}
