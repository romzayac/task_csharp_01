﻿using System;
using System.Text;

namespace Task2_Polynomial_
{
    class Polynomial
    {

        public Polynomial(int power, params double[] arrayOfCoefficients)
        {
            if (power < 0)
                throw new ArgumentOutOfRangeException("The power must be essential");
            if (arrayOfCoefficients.Length != power + 1)
                throw new ArgumentException("Count of coefficients must be equals power+1");

            _arrayOfCoefficients = arrayOfCoefficients;
            _power = power;
        }

        public double PolynomialValue(double argument)
        {
            double result=0;

            for (int i = 0; i <= Power; i++)
            {
                result += ArrayOfCoefficients[i] * Math.Pow(argument, i);
            }

            return result;
        }

        public static Polynomial Add(Polynomial first,Polynomial second)
        {
            //Determining the necessary data 
            Polynomial greaterPolynom = first.Power > second.Power ? first : second;
            int lessPower = first.Power > second.Power ? second.Power : first.Power;
            int indxOfGreaterPolynom = greaterPolynom.Power;
            int indxOfLessPolynom = lessPower;

            //Declareting the array of result coefficients
            double[] resultCoefficients = new double[greaterPolynom.Power + 1];

            //Assigning the coefficients of greater polynomial to result array of coefficients, while indices not be the same 
            while (indxOfGreaterPolynom!=lessPower)
            {
                resultCoefficients[indxOfGreaterPolynom] = greaterPolynom.ArrayOfCoefficients[indxOfGreaterPolynom];
                indxOfGreaterPolynom--;
            }

            //While indices are same and greater than zero, add coefficients of every polynomial and assing to result array of coefficient
            while(indxOfGreaterPolynom==indxOfLessPolynom && indxOfGreaterPolynom>=0)
            {
                resultCoefficients[indxOfGreaterPolynom] = first.ArrayOfCoefficients[indxOfGreaterPolynom] + second.ArrayOfCoefficients[indxOfLessPolynom];
                indxOfGreaterPolynom--;
                indxOfLessPolynom--;
            }

            return new Polynomial(greaterPolynom.Power, resultCoefficients);
        }

        public static Polynomial Sub(Polynomial first,Polynomial second)
        {
            double[] contraryCoefficients = new double[second.ArrayOfCoefficients.Length];

            //Filling the array of contrary coefficient to second polynomial coefficients
            for (int i = 0; i < contraryCoefficients.Length; i++)
            {
                contraryCoefficients[i] = -second.ArrayOfCoefficients[i];
            }

            //Returning the sum between first polynomial and polynomial cotrary to second (result will be substruction)
            return Add(first, new Polynomial(second.Power, contraryCoefficients));
        }

        public static Polynomial Mult(Polynomial first, Polynomial second)
        {
            Polynomial resultPolynomial = Polynomial.MultOnMonomial(first.ArrayOfCoefficients[first.Power], first.Power, second);

            //Determining result polynomial
            for (int i = first.Power-1; i >= 0; i--)
            {
                resultPolynomial = Polynomial.Add(resultPolynomial, Polynomial.MultOnMonomial(first.ArrayOfCoefficients[i], i, second));
            }

            return resultPolynomial;
        }

        private static Polynomial MultOnMonomial(double monomialCoefficient, int monomialPower, Polynomial value)
        {
            //Clone coefficients of polynomial to avoid problems with pointers in Mult(...) method
            double[] resultCoefficients = new double[value.ArrayOfCoefficients.Length];

            int currentPower = value.Power;
            int maxPower = currentPower + monomialPower;

            //Get the array of result coefficients
            for (int i = 0; i <= currentPower; i++) 
            {
                resultCoefficients[i] = monomialCoefficient * value.ArrayOfCoefficients[i];
            }

            //The array of result coefficient including zero in needed positions
            double[] correctCoefficients = new double[maxPower+1];
            //Index in "correctCoefficients" array
            int coeffIndx;

            for (int i = currentPower; i >= 0; i--)
            {
                coeffIndx = monomialPower + i;
                correctCoefficients[coeffIndx] = resultCoefficients[i];
            }

            return new Polynomial(maxPower, correctCoefficients);
        }

        public void Show()
        {
            StringBuilder resultString = new StringBuilder();

            resultString.AppendFormat("({0})x^{1} + ", ArrayOfCoefficients[Power], Power);

            for(int i=ArrayOfCoefficients.Length-2;i>0;i--)
            {
                resultString.AppendFormat("({0})x^{1} + ", ArrayOfCoefficients[i], i);
            }

            resultString.AppendFormat("{0} ", ArrayOfCoefficients[0]);

            Console.WriteLine(resultString);
        }


        public double[] ArrayOfCoefficients
        {
            get
            {
                return _arrayOfCoefficients;
            }
            private set
            {
                _arrayOfCoefficients = value;
            }
        }

        public int Power
        {
            get
            {
                return _power;
            }
            private set
            {
                _power = value;
            }
        }

        private int _power;
        private double[] _arrayOfCoefficients;

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\t\t*************Task2(Polinomial)************\n");
            try
            {
                Console.Write("A: ");
                Polynomial polynomialA = new Polynomial(2, 7, 5, 2);
                polynomialA.Show();

                Console.Write("B: ");
                Polynomial polynomialB = new Polynomial(5, 1, 1, -10, 1, 1, 1.6);
                polynomialB.Show();



                Console.WriteLine();
                Console.Write("AxB: ");
                Polynomial AMultToB = Polynomial.Mult(polynomialA, polynomialB);
                AMultToB.Show();
            }
            catch(ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace+" "+e.Message);
            }

            try
            {
                Console.Write("\n\nC: ");
                Polynomial polynomialC = new Polynomial(2, 6, 3, 1);
                polynomialC.Show();

                Console.Write("D: ");
                Polynomial polynomialD = new Polynomial(1,7,5);
                polynomialD.Show();

                Console.Write("\nC+D: ");
                Polynomial polynomialCPlusD = Polynomial.Add(polynomialC, polynomialD);
                polynomialCPlusD.Show();

                Console.Write("C-D: ");
                Polynomial polynomialCMinusD = Polynomial.Sub(polynomialC, polynomialD);
                polynomialCMinusD.Show();

                Console.Write("C*D: ");
                Polynomial polynomialCMultD = Polynomial.Mult(polynomialC, polynomialD);
                polynomialCMultD.Show();

            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace + " " + e.Message);
            }
        }
    }
}
