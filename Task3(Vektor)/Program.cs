﻿using System;
using System.Collections.Generic;

namespace Task3_Vektor_
{
    class Vector
    {

        public Vector() { }

        public Vector(params int[] values)
        {
            foreach (var item in values)
            {
                if(!IsValidVaule(item))
                    throw new ArgumentOutOfRangeException("The vector can contains values only from the set {0,1,2}");
            }
            currentVector.AddRange(values);
        }

        public void Add(int value)
        {
            if(!IsValidVaule(value))
                throw new ArgumentOutOfRangeException("The vector can contains values only from the set {0,1,2}");

            currentVector.Add(value);
        }

        public void Show()
        {
            foreach (var item in currentVector)
            {
                Console.Write(item + " ");
            }
        }

        public int CountOfTwoComponents()
        {
            int count = 0;
            foreach (var item in currentVector)
            {
                if (item == 2)
                {
                    count += 1;
                }
            }
            return count;
        }

        public bool IsOrthogonalTo(Vector other)
        {
            if (this.Size != other.Size)
                throw new ArgumentException("The argument vector must have same size with current");

            int dotProduct = 0;
            for(int i=0;i<Size;i++)
            {
                dotProduct += currentVector[i] * other[i];
            }

            return dotProduct == 0 ? true : false;
        }

        public Vector IntersectTo(Vector other)
        {
            if (this.Size != other.Size)
                throw new ArgumentException("The argument vector must have same size with current");

            if (this.IsOrthogonalTo(other))           
                throw new ArgumentException("These vectors are orthogonal and can`t be intersected");

            Vector resultVector = new Vector();

            for (int i = 0; i < Size; i++)
            {
                if((currentVector[i]==1 && other[i]==1) || (currentVector[i] == 1 && other[i] == 2) || (currentVector[i] == 2 && other[i] == 1))
                {
                    resultVector.Add(1);
                }
                else if((currentVector[i] == 0 && other[i] == 0) || (currentVector[i] == 0 && other[i] == 2) || (currentVector[i] == 2 && other[i] == 0) || (currentVector[i] == 1 && other[i] == 0) || (currentVector[i] == 0 && other[i] == 1))
                {
                    resultVector.Add(0);
                }
                else
                {
                    resultVector.Add(2);
                }
            }

            return resultVector;
        }

        public int this[int i]
        {
            get
            {
                return currentVector[i];
            }
            set
            {
                if (!IsValidVaule(value))
                    throw new ArgumentOutOfRangeException("The vector can contains values only from the set {0,1,2}");

                currentVector[i] = value;
            }
        }

        private bool IsValidVaule(int value)
        {
            return (value < 0 || value > 2) ? false : true;
        }

        public int Size { get { return currentVector.Count; } }

        private List<int> currentVector = new List<int>();

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\t************TASK-3(Vector)************\n");
            try
            {
                Vector vectorX = new Vector(1, 2, 0);
                Console.Write("Vector X: ");
                vectorX.Show();

                Vector vectorY = new Vector(0, 0, 2);
                Console.Write("\nVector Y: ");
                vectorY.Show();

                Console.WriteLine("\nVector X is orthogonal to Y: " + vectorX.IsOrthogonalTo(vectorY));

                Vector intersectionOfXY = vectorX.IntersectTo(vectorY);
                Console.Write("X intersect to Y: ");
                intersectionOfXY.Show();
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Vector vectorA = new Vector(2, 1, 2, 0, 1, 1, 2, 0, 0);
                Console.Write("\n\nVector A: ");
                vectorA.Show();

                Vector vectorB = new Vector(1, 0, 0, 1, 2, 1, 2, 2, 0);
                Console.Write("\nVector B: ");
                vectorB.Show();

                Console.WriteLine("\nVector A is orthogonal to vector B: " + vectorA.IsOrthogonalTo(vectorB));
                Console.WriteLine("Count of \"2\" components in A: " + vectorA.CountOfTwoComponents());

                Vector intersectionOfAB = vectorA.IntersectTo(vectorB);
                Console.Write("A intersect to B: ");
                intersectionOfAB.Show();

                Console.WriteLine();
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
