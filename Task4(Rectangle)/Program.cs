﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task4_Rectangle_
{
    class Rectangle
    {

        public Rectangle() { }

        public Rectangle(double leftTopX,double leftTopY,double rightBottomX,double rightBottomY)
        {
            if (leftTopX > rightBottomX || leftTopY < rightBottomY)
                throw new ArgumentException("Bad arguments");

            LeftTopX = leftTopX;
            LeftTopY = leftTopY;
            RightBottomX = rightBottomX;
            RightBottomY = rightBottomY;
        }

        // Add or subtitute value to move rectangle right or left and up or down
        public void Move(double upDown,double rightLeft)
        {
            if (upDown > 0)
                Console.Write("Move up on {0}, ", upDown);
            else if(upDown<0)
                Console.Write("Move down on {0}, ", Math.Abs(upDown));
            else
                Console.Write("Does not move up or down, ");

            if (rightLeft > 0)
                Console.Write("move right on {0}: ", rightLeft);
            else if (rightLeft < 0)
                Console.Write("move left on {0}: ", Math.Abs(rightLeft));
            else
                Console.Write("does not move right or left: ");

            RightBottomY += upDown;
            LeftTopY += upDown;

            RightBottomX += rightLeft;
            LeftTopX += rightLeft;
        }

        // Add or subtitute value to increase or decrease size of rectangle
        public void Resize(double addValue)
        {
            if(addValue>0)
                Console.Write("Increase size on {0}: ", addValue);
            else if (addValue < 0)
                Console.Write("Decrease size on {0}: ", Math.Abs(addValue));
            else
                Console.Write("The size does not changed: ");
            
            addValue = addValue / 2;

            LeftTopX += addValue;
            LeftTopY += addValue;
            RightBottomX += addValue;
            RightBottomY += addValue;
        }

        public override string ToString()
        {
            return String.Format("[Left top ({0},{1}), right bottom ({2},{3})]", LeftTopX, LeftTopY, RightBottomX, RightBottomY);
        }

        public override bool Equals(object obj)
        {
            return this.ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public static Rectangle CreateSmallestWhichCanIncludeTwo(Rectangle first, Rectangle second)
        {
            List<double> listOfX = new List<double>() { first.RightBottomX, first.LeftTopX, second.LeftTopX, second.RightBottomX };
            List<double> listOfY = new List<double>() { first.RightBottomY, first.LeftTopY, second.LeftTopY, second.RightBottomY };

            double smallestX = listOfX.Min();
            double smallestY = listOfY.Min();
            double biggestX = listOfX.Max();
            double biggestY = listOfY.Max();

            return new Rectangle(smallestX, biggestY, biggestX, smallestY);
        }

        public static Rectangle Intersect(Rectangle first, Rectangle second)
        {
            double maxLeftTopX = Math.Max(first.LeftTopX, second.LeftTopX);
            double minRightBottomX = Math.Min(first.RightBottomX, second.RightBottomX);
            double maxRightBottomY = Math.Max(first.RightBottomY, second.RightBottomY);
            double minLeftTopY = Math.Min(first.LeftTopY, second.LeftTopY);

            if (minRightBottomX >= maxLeftTopX && minLeftTopY >= maxRightBottomY)
            {
                return new Rectangle(maxLeftTopX, maxRightBottomY + (minLeftTopY - maxRightBottomY), maxLeftTopX + (minRightBottomX - maxLeftTopX), maxRightBottomY);
            }

            Console.WriteLine("does not exist");
            return null;
        }


        public double LeftTopX
        {
            get { return _leftTopX; }
            private set { _leftTopX = value; }
        }

        public double LeftTopY
        {
            get { return _leftTopY; }
            private set { _leftTopY = value; }
        }

        public double RightBottomX
        {
            get { return _rightBottomX; }
            private set { _rightBottomX = value; }
        }

        public double RightBottomY
        {
            get { return _rightBottomY; }
            private set { _rightBottomY = value; }
        }

        private double _leftTopX;
        private double _leftTopY;
        private double _rightBottomX;
        private double _rightBottomY;

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\t\t************Task4(Rectangle)************\n");

            try
            {
                Rectangle rectangle1 = new Rectangle(1, 4, 5, 1);
                Console.Write("Rectangle 1: ");
                Console.WriteLine(rectangle1);

                rectangle1.Move(-2, -5);
                Console.WriteLine(rectangle1);

                rectangle1.Resize(10);
                Console.WriteLine(rectangle1);

                Rectangle rectangle2 = new Rectangle(1, 0, 4, -1);
                Console.Write("\nRectangle 2: ");
                Console.WriteLine(rectangle2);

                Rectangle smallestWichIncludeTwo = Rectangle.CreateSmallestWhichCanIncludeTwo(rectangle1, rectangle2);
                Console.Write("\nThe smallest rectangle which include rectangle 1 and 2: ");
                Console.WriteLine(smallestWichIncludeTwo);

                Console.Write("\nIntersection between {0} and {1}: ", rectangle1.ToString(), rectangle2.ToString());
                Rectangle intrsctRectangle = Rectangle.Intersect(rectangle1, rectangle2);
                Console.WriteLine(intrsctRectangle);

                Rectangle rectangle3 = new Rectangle(0, 5, 2, -5);
                Console.Write("\nRectangle 3: ");
                Console.WriteLine(rectangle3);

                Console.Write("\nIntersection between {0} and {1}: ", rectangle1.ToString(), rectangle2.ToString());
                Rectangle intrsctRectangle2 = Rectangle.Intersect(rectangle1, rectangle3);
                Console.WriteLine(intrsctRectangle2);

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
